import QtQuick 2.12
import QtQuick.Controls 2.5
import "./DatabaseController.js" as DB

Item {
    height: childrenRect.height
    property bool ready: yearComboBox.currentIndex && makeComboBox.currentIndex && modelComboBox.currentIndex
    property bool partial: !ready && (yearComboBox.currentIndex + makeComboBox.currentIndex + modelComboBox.currentIndex) !== 0
    property alias year: yearComboBox.currentText
    property alias make: makeComboBox.currentText
    property alias model: modelComboBox.currentText

    Column {
        spacing: dp(10)
        anchors.horizontalCenter: parent.horizontalCenter
        ComboBox {
            id: yearComboBox
            editable: true
            validator: IntValidator {
                top: 2020
                bottom: 1900
            }

            function callUpdateMake(){
                if(!acceptableInput || currentIndex < 1){
                    return;
                }

                makeComboBox.updateMake(currentText);
            }

            onAcceptableInputChanged: callUpdateMake()
            onCurrentTextChanged: callUpdateMake()
        }

        ComboBox {
            id: makeComboBox
            enabled: yearComboBox.acceptableInput

            function updateMake(year){
                DB.getMake(year).then(function (makes){
                    makeComboBox.model = [].concat([qsTr('Make')],makes.map(function(x){return x.make}));
                    modelComboBox.model = []
                })
            }

            onCurrentTextChanged: {
                if(currentIndex < 1){
                    return;
                }

                modelComboBox.updateModel(yearComboBox.currentText, currentText)
            }
        }

        ComboBox {
            id: modelComboBox
            enabled: makeComboBox.currentIndex > 0

            function updateModel(year, make){
                DB.getModel(year, make).then(function (models){
                    modelComboBox.model = [].concat([qsTr('Model')],models.map(function(x){return x.model}));
                })
            }
        }
    }

    Component.onCompleted: {
        DB.getYears().then(function (years) {
            yearComboBox.model = [].concat([qsTr('Year')],years.map(function(x){return x.year}));
        })
    }

}
