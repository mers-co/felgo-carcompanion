.pragma library
.import QtQuick.LocalStorage 2.0 as Sql
.import "./VehicleData.js" as VehicleData

var db = dbGetHandle();

function initDb(db) {
    db.transaction(migration);
}

function migration(txr) {
    console.log('DB migration');

    let databaseSchemaSql = VehicleData.schema();
    let vehicleDataSql = VehicleData.data();

    txr.executeSql(databaseSchemaSql);
    txr.executeSql("CREATE INDEX I_VehicleModelYear_make on VehicleModelYear(make);");
    txr.executeSql("CREATE INDEX I_VehicleModelYear_model on VehicleModelYear(model);");
    txr.executeSql("CREATE INDEX I_VehicleModelYear_year on VehicleModelYear(year);");
    txr.executeSql(vehicleDataSql);
}

function dbGetHandle() {
    let db;
    try {
        db = Sql.LocalStorage.openDatabaseSync("CarCompanion", "", "X", 1000000, initDb)
    } catch (err) {
        console.log("Error opening database: " + err)
    }
    return db
}

function getVehicleData() {
    return new Promise(function (resolve, reject) {
        db.transaction(function (txr) {
            var results = txr.executeSql('SELECT * FROM VehicleModelYear');
            if (results.rows.length <= 0) {
                return resolve([])
            }
            let data = [];
            for (var i = 0; i < results.rows.length; i++) {
                data.push(results.rows.item(i));
            }
            resolve(data);
        })
    })
}

function getYears(){
    return new Promise(function (resolve, reject) {
        db.transaction(function (txr) {
            var results = txr.executeSql('SELECT DISTINCT year from VehicleModelYear order by year desc');
            if (results.rows.length <= 0) {
                return resolve([])
            }
            let data = [];
            for (var i = 0; i < results.rows.length; i++) {
                data.push(results.rows.item(i));
            }
            resolve(data);
        })
    })
}


function getMake(year){
    let selectMakeSql = `SELECT DISTINCT make from VehicleModelYear ${!!year ? `where year = ${year}`:''}`;
    console.log(selectMakeSql);
    return new Promise(function (resolve, reject) {
        db.transaction(function (txr) {
            var results = txr.executeSql(selectMakeSql);
            if (results.rows.length <= 0) {
                return resolve([])
            }
            let data = [];
            for (var i = 0; i < results.rows.length; i++) {
                data.push(results.rows.item(i));
            }
            resolve(data);
        })
    })
}

function getModel(year, make){
    let selectModelSql = `SELECT model from VehicleModelYear where year='${year}' and make='${make}'`;
    console.log(selectModelSql);
    return new Promise(function (resolve, reject) {
        db.transaction(function (txr) {
            var results = txr.executeSql(selectModelSql);
            if (results.rows.length <= 0) {
                return resolve([])
            }
            let data = [];
            for (var i = 0; i < results.rows.length; i++) {
                data.push(results.rows.item(i));
            }
            resolve(data);
        })
    })
}

//From: https://gist.github.com/agustinhaller/6181104
function makeid(length)
{
    if(!length){
        length = 5;
    }

    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for(var i=0; i < length; i++)
    {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
}




