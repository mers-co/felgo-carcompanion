import Felgo 3.0
import QtQuick 2.0
import "model"
import "logic"
import "pages"

import "helper"

App {

    //licenseKey: "<generate one from https://felgo.com/licenseKey>"

    // model
    //I rather use the inline object instantation only for UI-Renderizable elements
    property CarCompanionApp ccApp: CarCompanionApp {}
    property Storage appStorage: Storage {}

    // view
    Navigation {
        id: navigation

        // only enable if user is logged in
        // login page below overlays navigation then
        enabled: ccApp.user.logged

        NavigationItem {
            title: qsTr("Fuel-Ups")
            icon: IconType.list

            NavigationStack {
                splitView: tablet // use side-by-side view on tablets
                initialPage: FuelUps { }
            }
        }

        NavigationItem {
            title: qsTr("Vehicles")
            icon: IconType.car

            NavigationStack {
                splitView: tablet // use side-by-side view on tablets
                initialPage: VehiclesPage { }
            }
        }

        /*
        //Future use
        NavigationItem {
            title: qsTr("Maintenance")
            icon: IconType.wrench

            NavigationStack {
                splitView: tablet // use side-by-side view on tablets
                initialPage: MaintenancePage { }
            }
        }*/

        NavigationItem {
            title: qsTr("Profile") // use qsTr for strings you want to translate
            icon: IconType.circle

            NavigationStack {
                initialPage: ProfilePage {
                    onLogoutClicked: {
                        ccApp.logout()

                        // jump to main page after logout
                        navigation.currentIndex = 0
                        navigation.currentNavigationItem.navigationStack.popAllExceptFirst()
                    }
                }
            }
        }
    }

    // login page lies on top of previous items and overlays if user is not logged in
    LoginPage {
        visible: opacity > 0
        enabled: visible
        opacity: ccApp.user.logged ? 0 : 1 // hide if user is logged in

        Behavior on opacity { NumberAnimation { duration: 250 } } // page fade in/out
    }

}
