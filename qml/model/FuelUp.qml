import QtQuick 2.0

QtObject {
    property string id
    property string vehicleId
    property double odometer
    property double timestamp   //Epoch UTC
    property string locationLat
    property string locationLon
    property int volume         //In iters
    property int price          //Price per unit of volume

    property var dateTime
}
