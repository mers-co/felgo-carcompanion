import QtQuick 2.0

QtObject {
    property bool logged: false
    property string name
    property string email
    property bool useMetric: true
}
