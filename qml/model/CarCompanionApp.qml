import QtQuick 2.0
import Felgo 3.0

import "../logic/CarCompanionApp.js" as Logic
import "../model"

QtObject {
    property bool busy: false //Future use
    property User user: User {}
    readonly property Storage _storage: Storage {}
    property ListModel vehicles: ListModel {}
    property var vehiclesList: ([])
    property Vehicle currentVehicle: Vehicle {}

    function setup(userSetup){return Logic.setup(userSetup)}
    function logout(){return Logic.logout()}

    function storeVehicles(){return Logic.storeVehicles()}
    function getVehicleById(id){return Logic.getVehicleById(id)}
    function addVehicle(vehicle){return Logic.addVehicle(vehicle)}
    function addFuelUp(fuelUp){return Logic.addFuelUp(fuelUp)}

    Component.onCompleted: Logic.init();
}
