import QtQuick 2.0
import Felgo 3.0

QtObject {
    property string id
    property int year
    property string make
    property string model
    property string nickname
    property double odometer: -1 //We store verything in Metric system
    property double trip: -1

    property JsonListModel fuelUps: JsonListModel {}

    onIdChanged: {
        objectName = 'Vehicle_' + id;
    }
}
