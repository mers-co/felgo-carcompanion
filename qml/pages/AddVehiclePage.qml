import QtQuick 2.0
import Felgo 3.0
import "../helper"

Page {
    id: page
    title: qsTr('Add vehicle')
    backNavigationEnabled: !makeModelSelect.partial

    Column {
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.margins: dp(20)
        width: parent.width
        spacing: dp(20)

        MakeModelSelect {
            id: makeModelSelect
            width: parent.width * 0.8
            height: parent.height * 0.3
            anchors.horizontalCenter: parent.horizontalCenter
        }

        AppTextField {
            id: nicknameTextField
            anchors.horizontalCenter: parent.horizontalCenter
            placeholderText: qsTr('Nickname')
        }

        AppTextField {
            id: odometerTextField
            anchors.horizontalCenter: parent.horizontalCenter
            placeholderText: qsTr('Odometer')
            validator: DoubleValidator{
                bottom: 0
            }

            AppText {
                anchors.right: parent.right
                anchors.rightMargin: dp(10)
                text: ccApp.user.useMetric ? qsTr('km') : qsTr('miles')
            }
        }

        AppButton {
            anchors.horizontalCenter: parent.horizontalCenter
            enabled: makeModelSelect.ready
            text: qsTr('Add')
            onClicked: {

                ccApp.addVehicle({
                    year: makeModelSelect.year,
                    make: makeModelSelect.make,
                    model: makeModelSelect.model,
                    nickname: nicknameTextField.text,
                    odometer: odometerTextField.text !== '' ? odometerTextField.text/(!ccApp.user.useMetric ? 0.621371 : 1) : undefined
                })

                page.navigationStack.pop();
            }
        }

        AppButton {
            anchors.horizontalCenter: parent.horizontalCenter
            enabled: !page.backNavigationEnabled
            visible: enabled
            text: qsTr('Cancel')
            onClicked: {
                page.navigationStack.pop();
            }
        }
    }
}
