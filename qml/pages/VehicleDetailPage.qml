import Felgo 3.0
import QtQuick 2.0

import "../model/"

Page {
    id: page

    property Vehicle vehicle: Vehicle {}

    Column {
        y: spacing
        width: parent.width - 2 * spacing
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: dp(Theme.navigationBar.defaultBarItemPadding)

        Repeater {
            enabled: parent.visible
            model: ['year','make','model']

            // Text Item to show each property - value pair
            AppText {
                property string propName: modelData //Translation is missing...
                property string value: vehicle[propName]

                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                height: implicitHeight

                text: "<strong>"+propName+":</strong> " + value
                wrapMode: AppText.WrapAtWordBoundaryOrAnywhere
            }
        }

        AppTextField {
            width: parent.width
            anchors.horizontalCenter: parent.horizontalCenter
            height: implicitHeight
            placeholderText: qsTr('Nickname')
            text: vehicle.nickname

            onTextChanged: {
                vehicle.nickname = text;
            }
        }

        Item{
            id: odometerItem
            height: childrenRect.height
            width: parent.width

            property bool editOdoEnabled: false
            Row {
                spacing: dp(5)
                AppText {
                    text: '<b>'+qsTr('Odometer')+'</b>'
                }

                AppText {
                    visible: !odometerItem.editOdoEnabled
                    width: visible ? implicitWidth : 0
                    text: (vehicle.odometer > 0 ? (vehicle.odometer*(!ccApp.user.useMetric ? 0.621371 : 1)).toFixed(1) : '') + ' ' + (ccApp.user.useMetric ? qsTr('km') : qsTr('miles'))
                }

                AppTextField {
                    visible: odometerItem.editOdoEnabled
                    width: visible ? odometerItem.width*0.6 : 0
                    height: implicitHeight
                    placeholderText: qsTr('Odometer')
                    anchors.verticalCenter: parent.verticalCenter
                    text:  (vehicle.odometer > 0 ? (vehicle.odometer*(!ccApp.user.useMetric ? 0.621371 : 1)).toFixed(1) : undefined)

                    onTextChanged: {
                        if(!odometerItem.editOdoEnabled){
                            return;
                        }

                        vehicle.odometer = text/(!ccApp.user.useMetric ? 0.621371 : 1);
                    }
                }


            }
            Icon {
                anchors.right: parent.right
                anchors.rightMargin: dp(5)
                icon: IconType.cog
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        odometerItem.editOdoEnabled = !odometerItem.editOdoEnabled;
                    }
                }
            }
        }


        Row{
            spacing: dp(40)
            anchors.horizontalCenter: parent.horizontalCenter
            AppText {
                anchors.verticalCenter: parent.verticalCenter
                text: '<b>' + qsTr('Current') + '</b>'
            }

            AppSwitch {
                anchors.verticalCenter: parent.verticalCenter
                checked: ccApp.currentVehicle.id === vehicle.id
                enabled: ccApp.currentVehicle.id !== vehicle.id
                onCheckedChanged: {
                    ccApp.currentVehicle = ccApp.getVehicleById(vehicle.id);
                }
            }
        }
    }

    Component.onCompleted: {
         title = vehicle.make + ' ' + vehicle.model
    }

    Component.onDestruction: {
        ccApp.storeVehicles();
    }
}
