import Felgo 3.0
import QtQuick 2.0
import QtQuick.Layouts 1.1

import "../helper"

Page {
    id: loginPage
    title: qsTr("Setup")

    backgroundColor: Qt.rgba(
                         0, 0, 0,
                         0.75) // page background is translucent, we can see other items beneath the page
    useSafeArea: false // do not consider safe area insets of screen

    // login form background
    Rectangle {
        id: loginForm
        anchors.centerIn: parent
        color: "white"
        width: content.width + dp(48)
        height: content.height + dp(16)
        radius: dp(4)
    }

    // login form content
    GridLayout {
        id: content
        anchors.centerIn: loginForm
        columnSpacing: dp(20)
        rowSpacing: dp(8)
        columns: 2

        // headline
        AppText {
            Layout.topMargin: dp(4)
            Layout.bottomMargin: dp(8)
            Layout.columnSpan: 2
            Layout.alignment: Qt.AlignHCenter
            text: qsTr('Lets begin!')
        }

        // email text and field
        AppText {
            text: qsTr("Name")
            font.pixelSize: sp(12)
        }

        AppTextField {
            id: txtUsername
            Layout.preferredWidth: dp(200)
            showClearButton: true
            font.pixelSize: sp(14)
            borderColor: Theme.tintColor
            borderWidth: !Theme.isAndroid ? dp(2) : 0
        }

        AppText {
            text: qsTr("E-mail")
            font.pixelSize: sp(12)
        }

        AppTextField {
            id: txtUserEmail
            Layout.preferredWidth: dp(200)
            showClearButton: true
            font.pixelSize: sp(14)
            borderColor: Theme.tintColor
            borderWidth: !Theme.isAndroid ? dp(2) : 0
            validator: RegExpValidator { regExp:/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/ }
        }

        AppText {
            text: qsTr("Choose your vehicle")
            font.pixelSize: sp(14)
            Layout.topMargin: dp(4)
            Layout.bottomMargin: dp(8)
            Layout.columnSpan: 2
            Layout.alignment: Qt.AlignHCenter
        }

        MakeModelSelect {
            id: makeModelSelect
            Layout.preferredWidth: dp(200)
            Layout.columnSpan: 2
            Layout.alignment: Qt.AlignHCenter
        }

        // column for buttons, we use column here to avoid additional spacing between buttons
        Column {
            Layout.fillWidth: true
            Layout.columnSpan: 2
            Layout.topMargin: dp(12)

            // buttons
            AppButton {
                text: qsTr("Continue")
                flat: false
                anchors.horizontalCenter: parent.horizontalCenter
                enabled: makeModelSelect.ready && txtUsername.text !== '' && txtUserEmail.acceptableInput

                onClicked: {
                    loginPage.forceActiveFocus();
                    ccApp.setup({
                        user: {
                            name: txtUsername.text,
                            email: txtUserEmail.text
                        },
                        vehicle:{
                            year: makeModelSelect.year,
                            make: makeModelSelect.make,
                            model: makeModelSelect.model
                        }
                    })
                }
            }
        }
    }
}
