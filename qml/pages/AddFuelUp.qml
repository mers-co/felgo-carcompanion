import QtQuick 2.0
import Felgo 3.0

Page {

    Column {
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.margins: dp(20)
        width: parent.width
        spacing: dp(25)

        AppText {
            id: currenVehicleText
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: dp(20)
            font.bold: Font.DemiBold
            text:   ccApp.currentVehicle.nickname !== '' ?  ccApp.currentVehicle.nickname : (ccApp.currentVehicle.year + ' ' + ccApp.currentVehicle.make + ' ' + ccApp.currentVehicle.model)
        }

        AppText {
            id: currenVehicleOdometerText
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: dp(16)
            font.bold: Font.DemiBold
            text: (ccApp.currentVehicle.odometer > 0 ? ccApp.currentVehicle.odometer*(ccApp.user.useMetric ? 1 : 0.621371) :' -- ') +' ' + (ccApp.user.useMetric ? qsTr('km') : qsTr('miles'))
        }

        AppTextField {
            id: priceTextField
            anchors.horizontalCenter: parent.horizontalCenter
            placeholderText: qsTr('Price per') + ' ' + (ccApp.user.useMetric ? qsTr('liter') : qsTr('gal'))
            validator: DoubleValidator {
                bottom: 0
            }
        }

        AppTextField {
            id: volumeTextField
            anchors.horizontalCenter: parent.horizontalCenter
            placeholderText: qsTr('Volume') + ' ' + (ccApp.user.useMetric ? qsTr('liters') : qsTr('gal'))
            validator: DoubleValidator {
                bottom: 0
            }
        }


        AppTextField {
            id: odometerTextField
            anchors.horizontalCenter: parent.horizontalCenter
            placeholderText: qsTr('Odometer')
            validator: DoubleValidator{
                bottom: 0
            }

            AppText {
                anchors.right: parent.right
                anchors.rightMargin: dp(10)
                text: ccApp.user.useMetric ? qsTr('km') : qsTr('miles')
            }
        }

        AppText{
            anchors.horizontalCenter: parent.horizontalCenter
            text: '<b>'+qsTr('Trip')+'</b> ' + (odometerTextField.text !== '' ? odometerTextField.text - ccApp.currentVehicle.odometer : ' -- ')
        }

        AppButton {
            anchors.horizontalCenter: parent.horizontalCenter
            enabled: priceTextField.text !== '' && odometerTextField.text !== '' && volumeTextField.text !== ''
            text: qsTr('Add')
            onClicked: {
                ccApp.addFuelUp({
                    price: priceTextField.text,
                    odometer: odometerTextField.text,
                    volume: volumeTextField.text
                })
                page.navigationStack.pop();
            }
        }

        AppButton {
            anchors.horizontalCenter: parent.horizontalCenter
            enabled: !page.backNavigationEnabled
            visible: enabled
            text: qsTr('Cancel')
            onClicked: {
                page.navigationStack.pop();
            }
        }
    }

}
