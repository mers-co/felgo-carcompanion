import Felgo 3.0
import QtQuick 2.0

Page {
    id: page
    title: qsTr("Vehicles") // use qsTr for strings you want to translate

    rightBarItem: NavigationBarRow {
        ActivityIndicatorBarItem {
            enabled: ccApp.busy
            visible: enabled
            showItem: showItemAlways
        }

        IconButtonBarItem {
            icon: IconType.plus
            showItem: showItemAlways
            onClicked: {
                page.navigationStack.popAllExceptFirstAndPush(addVehiclePageComponent)
            }
        }
    }

    AppListView {
        id: listView
        anchors.fill: parent

        model: ccApp.vehicles
        delegate: SimpleRow {
            //This is my prefered way, but QtCreator takes it as an error
            text: {`${year} ${make} ${model}`}

            onSelected: {
                page.navigationStack.popAllExceptFirstAndPush(detailPageComponent, { vehicle: ccApp.getVehicleById(id) })
            }

            Icon {
                anchors.right: parent.right
                anchors.rightMargin: dp(5)
                anchors.verticalCenter: parent.verticalCenter
                visible: id == ccApp.currentVehicle.id
                icon: IconType.car
            }
        }
    }

    // component for creating detail pages
    Component {
        id: detailPageComponent
        VehicleDetailPage { }
    }

    Component {
        id: addVehiclePageComponent
        AddVehiclePage { }
    }
}
