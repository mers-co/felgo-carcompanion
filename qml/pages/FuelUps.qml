import Felgo 3.0
import QtQuick 2.0

Page {
    id: page
    title: qsTr("Fuel Ups") // use qsTr for strings you want to translate

    property SortFilterProxyModel filteredSortedFuelUps: SortFilterProxyModel{
        sourceModel: ccApp.currentVehicle.fuelUps
        sorters: RoleSorter { 
            roleName: "timestamp"
            sortOrder: Qt.DescendingOrder
        }
    }

    rightBarItem: NavigationBarRow {
        // network activity indicator
        ActivityIndicatorBarItem {
            enabled: ccApp.busy
            visible: enabled
            showItem: showItemAlways // do not collapse into sub-menu on Android
        }

        // add new todo
        IconButtonBarItem {
            icon: IconType.plus
            showItem: showItemAlways
            onClicked: {
                page.navigationStack.popAllExceptFirstAndPush(addFuelUpComponent)
            }
        }
    }

    AppText {
        id: currenVehicleText
        anchors.top: parent.top
        anchors.topMargin: dp(20)
        anchors.horizontalCenter: parent.horizontalCenter
        font.pixelSize: dp(20)
        font.bold: Font.DemiBold
        text:   ccApp.currentVehicle.nickname !== '' ?  ccApp.currentVehicle.nickname : (ccApp.currentVehicle.year + ' ' + ccApp.currentVehicle.make + ' ' + ccApp.currentVehicle.model)
    }

    // show sorted/filterd todos of data model
    AppListView {
        id: listView
        anchors.top: currenVehicleText.bottom
        anchors.topMargin: dp(10)
        anchors.bottom: parent.bottom
        // the model specifies the data for the list view
        model: filteredSortedFuelUps

        // the delegate is the template item for each entry of the list
        delegate: SimpleRow {
            text: volume + ' '+ qsTr('liters') + ' - ' + qsTr('price') + ' '+ (price*volume).toLocaleCurrencyString()

            // push detail page when selected, pass chosen todo id
            onSelected: page.navigationStack.popAllExceptFirstAndPush(detailPageComponent, { todoId: model.id })

            AppText {
               anchors.right: parent.right
               anchors.rightMargin: dp(10)
               anchors.verticalCenter: parent.verticalCenter
               text: Qt.formatDateTime(new Date(timestamp), "dd/MM/yy hh:mm")
            }
        }
    }

    // component for creating detail pages
    Component {
        id: detailPageComponent
        FuelUpDetail { }
    }

    Component {
        id: addFuelUpComponent
        AddFuelUp { }
    }
}
