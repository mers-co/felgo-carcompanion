import Felgo 3.0
import QtQuick 2.12

Page {
    title: qsTr("Profile")

    signal logoutClicked

    Flickable{
        anchors.top: parent.top
        anchors.topMargin: dp(10)
        anchors.bottom: logoutButton.top
        anchors.bottomMargin: dp(20)
        width: parent.width
        contentHeight: childrenRect.height
        clip: true

        Column {
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: dp(10)
            Rectangle {
                width: dp(210)
                height: width
                radius: width/2
                color: 'lightgray'
            }

            AppText {
                text: ccApp.user.name
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: dp(20)
            }

            AppText {
                text: ccApp.user.email
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: dp(20)
            }

            Row {
                anchors.horizontalCenter: parent.horizontalCenter
                spacing: dp(10)
                AppText {
                    text: '<b>' + qsTr('Units') + '</b>'
                }
                AppSwitch {
                    checked: ccApp.user.useMetric
                    onCheckedChanged: {
                        ccApp.user.useMetric = checked;
                    }
                }
                AppText {
                    text: ccApp.user.useMetric ? qsTr('Metric') : qsTr('Imperial')
                }
            }

        }
    }



    AppButton {
        id: logoutButton
        anchors.bottom: parent.bottom
        anchors.bottomMargin: dp(30)
        anchors.horizontalCenter: parent.horizontalCenter
        text: qsTr("Logout")
        onClicked: logoutClicked()
    }
}
