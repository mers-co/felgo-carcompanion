.import "../helper/DatabaseController.js" as DB
var vehicleModel = Qt.createComponent("../model/Vehicle.qml");
var fuelUpModel = Qt.createComponent("../model/FuelUp.qml");

function init() {
    console.log('CarCompanion is starting!');
    let existingUser = appStorage.getValue("user");
    if(!!existingUser){
        Object.assign(user, JSON.parse(existingUser));
        user.logged = true;
        let vehiclesListDB = JSON.parse(appStorage.getValue('vehicles'));
        let currentVehicleId = appStorage.getValue('currentVehicleId');

        vehiclesListDB.forEach(vehicle => {
            let vehicleToAppend = vehicleModel.createObject(ccApp, vehicle);
            vehicles.append(vehicleToAppend);
            vehiclesList.push(vehicleToAppend)
            console.log(JSON.stringify(vehiclesList))
            if(vehicle.id == currentVehicleId){
                console.log('Current vehicle ID', currentVehicleId, JSON.stringify(vehicle))
                currentVehicle = vehicleToAppend;
            }
        })
    }
}

function setup(userSetup){
    console.log(JSON.stringify(userSetup, null, 2))

    Object.assign(user, userSetup.user);
    user.logged = true;

    let initialVehicle = vehicleModel.createObject(ccApp, userSetup.vehicle);
    currentVehicle = initialVehicle;
    initialVehicle.id = DB.makeid();
    vehicles.append(initialVehicle);
    vehiclesList.push(initialVehicle)

    appStorage.setValue('user',JSON.stringify(user));
    appStorage.setValue('vehicles',JSON.stringify([initialVehicle]));
    appStorage.setValue('currentVehicleId',initialVehicle.id);
}

function storeVehicles(){
    appStorage.setValue('vehicles',JSON.stringify(vehiclesList));
    appStorage.setValue('currentVehicleId',currentVehicle.id);
}

function logout(){
    user.logged = false;
    vehicles.clear();
    appStorage.setValue('user',undefined);
    appStorage.setValue('vehicles',undefined);
    appStorage.setValue('currentVehicleId',undefined);
}

function getVehicleById(id){
    let returnVehicle = vehiclesList.find(v => v.id == id);
    if(!returnVehicle) {
        returnVehicle = vehicleModel.createObject(ccApp, {id});
    }
    return returnVehicle;
}

function addVehicle(vehicle){
    let newVehicle = vehicleModel.createObject(ccApp, vehicle);
    newVehicle.id = DB.makeid();
    vehicles.append(newVehicle);
    vehiclesList.push(newVehicle);
    storeVehicles();
}

function addFuelUp(fuelUp){
    let newFuelUp = fuelUpModel.createObject(currentVehicle, fuelUp);
    newFuelUp.id = DB.makeid();
    newFuelUp.timestamp = Date.now();
    currentVehicle.fuelUps.append(JSON.parse(JSON.stringify(newFuelUp)));
    console.log('Done', currentVehicle.fuelUps.count);
}
